FROM centbee/cordova

ARG BUILD_DATE
ARG BUILD_VERSION
ARG VCS_REF

LABEL maintainer="lorien@centbee.com" \
	org.label-schema.build-date=$BUILD_DATE \
	org.label-schema.version=$BUILD_VERSION \
	org.label-schema.vcs-ref=$VCS_REF \
	org.label-schema.schema-version="1.0" \
	org.label-schema.name="centbee/ionic" \
	org.label-schema.description="Ionic Image" \
	org.label-schema.vendor="Lorien Gamaroff (Centbee)" \
	org.label-schema.license="MIT" 

ENV IONIC_VERSION 5.2.3

RUN apt-get update && apt-get install -y git bzip2 openssh-client && \
	npm i -g --unsafe-perm ionic@${IONIC_VERSION} && \
	ionic --no-interactive config set -g daemon.updates false && \
	rm -rf /var/lib/apt/lists/* && apt-get clean