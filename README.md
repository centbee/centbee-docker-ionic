# Centbee Ionic image

### Pull newest build from Docker Hub
```
docker pull centbee/ionic:latest
```

### Run image
```
docker run -it centbee/ionic:latest bash
```

### Use as base image
```Dockerfile
FROM centbee/ionic:latest
```
